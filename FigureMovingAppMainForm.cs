﻿//-----------------------------------------------------------------------
// <copyright file="FigureMovingAppMainForm.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------

namespace FigureMovingApp
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Resources;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using FigureMovingApp.Common.Enums;
    using FigureMovingApp.Common.EventArgs;
    using FigureMovingApp.Common.Exceptions;
    using FigureMovingApp.Common.Extensions;
    using FigureMovingApp.Models;
    using FigureMovingApp.Services;
    using Figures;
    using Rectangle = Figures.Rectangle;

    /// <summary>
    /// Logic for form of Application.
    /// </summary>
    public partial class FigureMovingAppMainForm : Form
    {
        /// <summary>
        /// Locker for threads.
        /// </summary>
        private static object locker = new object();

        /// <summary>
        /// List of figures that must be present in mainPictureBox.
        /// </summary>
        private readonly List<Figure> figures;

        /// <summary>
        /// Instance of resource manager.
        /// </summary>
        private readonly ResourceManager resourceManager;

        /// <summary>
        /// Instance of serialization service.
        /// </summary>
        private readonly SerializeService serializeService;

        /// <summary>
        /// Selected figure in Tree View.
        /// </summary>
        private Figure selectedFigure;

        /// <summary>
        /// Instance of culture info(localization).
        /// </summary>
        private CultureInfo cultureInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="FigureMovingAppMainForm" /> class.
        /// </summary>
        public FigureMovingAppMainForm()
        {
            Logger.InitLogger();
            this.resourceManager = new System.Resources.ResourceManager("FigureMovingApp.Properties.Resources", typeof(FigureMovingAppMainForm).Assembly);
            this.serializeService = new SerializeService();
            this.InitializeComponent();
            this.languageComboBox.Items.Add(this.resourceManager.GetString("English_ComboBox_item", this.cultureInfo));
            this.languageComboBox.Items.Add(this.resourceManager.GetString("Russian_ComboBox_item", this.cultureInfo));
            this.languageComboBox.SelectedIndex = 0;

            this.figures = new List<Figure>();
        }

        /// <summary>
        /// Gets or sets culture.
        /// </summary>
        public CultureInfo CultureInfo
        {
            get
            {
                return this.cultureInfo;
            }

            set
            {
                this.cultureInfo = value;
                this.SetLabelsText();
            }
        }

        /// <summary>
        /// Calls by event Paint in PictureBoxMain. Draws and moves figures in mainPictureBox.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of main PictureBox event Paint.</param>
        private void PictureBoxMain_Paint(object sender, PaintEventArgs e)
        {
            var threads = new List<Thread>();

            foreach (Figure f in this.figures)
            {
                var drawThread = new Thread(new ParameterizedThreadStart(this.DrawFigure));
                drawThread.Start(new DrawerModel(f, e.Graphics));
                threads.Add(drawThread);

                try
                {
                    f.Move(this.figures);
                }
                catch (Exception<OutOfAreaExceptionArgs> ex)
                {
                    Logger.Log.Info($"Figure {ex.Args.Figure.Name} is out of PictureBox in point{ex.Args.Figure.Coordinates}");
                    if (ex.Args.Figure.Coordinates.X > this.pictureBoxMain.Width)
                    {
                        f.Coordinates = new Point(this.pictureBoxMain.Width - 30, f.Coordinates.Y);
                    }

                    if (ex.Args.Figure.Coordinates.Y > this.pictureBoxMain.Height)
                    {
                        f.Coordinates = new Point(f.Coordinates.X, this.pictureBoxMain.Height - 30);
                    }
                }
            }

            if (threads.FirstOrDefault(x => x.ThreadState == ThreadState.Running) != null)
            {
                foreach (Thread t in threads.Where(x => x.ThreadState == ThreadState.Running))
                {
                    t.Join();
                }
            }
        }

        /// <summary>
        /// Calls by event Paint in CircleButton. Draws Circle in button.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of CircleButton event Paint.</param>
        private void CircleButton_Paint(object sender, PaintEventArgs e)
        {
            var circle = new Circle();
            circle.Draw(e.Graphics, this.buttonCreateCircle);
        }

        /// <summary>
        /// Calls by event Paint in TriangleButton. Draws Circle in button.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of TriangleButton event Paint.</param>
        private void TriangleButton_Paint(object sender, PaintEventArgs e)
        {
            var triangle = new Triangle();
            triangle.Draw(e.Graphics, this.buttonCreateTriangle);
        }

        /// <summary>
        /// Calls by event Paint in RectangleButton. Draws Circle in button.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of RectangleButton event Paint.</param>
        private void RectangleButton_Paint(object sender, PaintEventArgs e)
        {
            var rectangle = new Rectangle();
            rectangle.Draw(e.Graphics, this.buttonCreateRectangle);
        }

        /// <summary>
        /// Calls after click on buttonCreateCircle. Adds Circle to figures and to treeView.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of buttonCreateCircle click-event.</param>
        private void ButtonCreateCircle_Click(object sender, EventArgs e)
        {
            var circle = new Circle(this.pictureBoxMain);
            this.GenarateFigureName(circle);
            this.figures.Add(circle);
            this.AddFigureToTreeView(circle);

            this.timer.Enabled = true;
        }

        /// <summary>
        /// Calls after click on buttonCreateRectangle. Adds Rectangle to figures and to treeView.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of buttonCreateRectangle click-event.</param>
        private void ButtonCreateRectangle_Click(object sender, EventArgs e)
        {
            var rectangle = new Rectangle(this.pictureBoxMain);
            this.GenarateFigureName(rectangle);
            this.figures.Add(rectangle);
            this.AddFigureToTreeView(rectangle);

            this.timer.Enabled = true;
        }

        /// <summary>
        /// Calls after click on buttonCreateTriangle. Adds Triangle to figures and to treeView.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of buttonCreateTriangle click-event.</param>
        private void ButtonCreateTriangle_Click(object sender, EventArgs e)
        {
            Triangle triangle = new Triangle(this.pictureBoxMain);
            this.GenarateFigureName(triangle);
            this.figures.Add(triangle);
            this.AddFigureToTreeView(triangle);

            this.timer.Enabled = true;
        }

        /// <summary>
        /// Clears Figures-list and TreeView.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of buttonClear click-event.</param>
        private void ButtonClear_Click(object sender, EventArgs e)
        {
            this.figures.Clear();
            this.treeViewFigures.Nodes.Clear();
            this.selectedFigure = null;
            this.runStopButton.Enabled = false;
            this.plusButton.Enabled = false;
            this.minusButton.Enabled = false;
        }

        /// <summary>
        /// Refresh pictureBoxMain. Calls after tick of timer.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of timer tick-event.</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            this.pictureBoxMain.Refresh();
        }

        /// <summary>
        /// Set name for figure.
        /// </summary>
        /// <param name="figure">Figure for setting name.</param>
        private void GenarateFigureName(Figure figure)
        {
            if (this.figures.Count == 0)
            {
                figure.Name = figure.GetType().Name + 1;
                return;
            }

            int currentFiguresCount = 0;

            for (int i = 0; i < this.figures.Count; i++)
            {
                if (this.figures[i].GetType() == figure.GetType())
                {
                    currentFiguresCount++;
                }
            }

            figure.Name = figure.GetType().Name + (currentFiguresCount + 1);
        }

        /// <summary>
        /// Uses to add figure to TreeViewNodes.
        /// </summary>
        /// <param name="figure">Figure that need to add to TreeView.</param>
        private void AddFigureToTreeView(Figure figure)
        {
            if (this.treeViewFigures.Nodes.Count == 0)
            {
                this.treeViewFigures.Nodes.Add(this.resourceManager.GetString("Figures_Text", this.cultureInfo));
                this.treeViewFigures.Nodes[0].Nodes.Add(this.resourceManager.GetString("Circles_Text", this.cultureInfo));
                this.treeViewFigures.Nodes[0].Nodes.Add(this.resourceManager.GetString("Rectangles_Text", this.cultureInfo));
                this.treeViewFigures.Nodes[0].Nodes.Add(this.resourceManager.GetString("Triangles_Text", this.cultureInfo));
            }

            if (figure is Circle)
            {
                this.treeViewFigures.Nodes[0].Nodes[0].Nodes.Add(figure.Name);
            }

            if (figure is Rectangle)
            {
                this.treeViewFigures.Nodes[0].Nodes[1].Nodes.Add(figure.Name);
            }

            if (figure is Triangle)
            {
                this.treeViewFigures.Nodes[0].Nodes[2].Nodes.Add(figure.Name);
            }
        }

        /// <summary>
        /// Show open dialog and select file for deserialize.
        /// </summary>
        /// <returns>Stream of deserialize file and format of file.</returns>
        private (FileStream, SerializeFormat) ShowOpenDialog()
        {
            this.openFileDialog.ShowDialog();
            var fileStream = (FileStream)this.openFileDialog.OpenFile();
            var fileType = this.GetSerializeFormat(this.openFileDialog.FileName.Split('.')[1]);
            return (fileStream, fileType);
        }

        /// <summary>
        /// Open figure file and deserialize.
        /// </summary>
        /// <typeparam name="TFigureType">Type of figure that need to deserialize.</typeparam>
        private void SelectFigure<TFigureType>() where TFigureType : class
        {
            (var fileStream, var fileType) = this.ShowOpenDialog();
            var deserializedObject = this.serializeService.DeserializeObject<TFigureType>(fileType, fileStream);
            if (deserializedObject is Figure)
            {
                (deserializedObject as Figure).SetPictureBox(this.pictureBoxMain);
                this.figures.Add(deserializedObject as Figure);
                this.timer.Enabled = true;
                this.AddFigureToTreeView(deserializedObject as Figure);
            }
        }

        /// <summary>
        /// Open point file and deserialize.
        /// </summary>
        private void SelectPoint()
        {
            (var fileStream, var fileType) = this.ShowOpenDialog();
            var deserializedObject = this.serializeService.DeserializeObject<Point>(fileType, fileStream);

            var selectPositionForFigureForm = new SelectPositionForFigureForm(this.figures, this.cultureInfo, this.resourceManager);
            selectPositionForFigureForm.ShowDialog();

            if (selectPositionForFigureForm.DialogResult == DialogResult.OK)
            {
                this.figures.FirstOrDefault(x => x.Id == selectPositionForFigureForm.AlterableFigure.Id).Coordinates = deserializedObject;
            }
        }

        /// <summary>
        /// Show figureActionForm.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of SaveToolStripMenuItem click-event.</param>
        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var figureActionsForm = new FigureActionsForm(this.figures, this.cultureInfo, this.resourceManager);
            figureActionsForm.Show();
        }

        /// <summary>
        /// Select type(circle) of deserialize object.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of CircleToolStripMenuItem click-event.</param>
        private void CircleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SelectFigure<Circle>();
        }

        /// <summary>
        /// Select type(Triangle) of deserialize object.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of TriangleToolStripMenuItem click-event.</param>
        private void TriangleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SelectFigure<Triangle>();
        }

        /// <summary>
        /// Select type(Rectangle) of deserialize object.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of RectangleToolStripMenuItem click-event.</param>
        private void RectangleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SelectFigure<Rectangle>();
        }

        /// <summary>
        /// Select type(Point) of deserialize object.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of PositionToolStripMenuItem click-event.</param>
        private void PositionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SelectPoint();
        }

        /// <summary>
        /// Run or stop figure.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of RunStopButton click-event.</param>
        private void RunStopButton_Click(object sender, EventArgs e)
        {
            if (this.selectedFigure.IsMoving)
            {
                this.selectedFigure.IsMoving = false;
                this.runStopButton.Text = this.resourceManager.GetString("Run_Text", this.cultureInfo);
                return;
            }

            this.selectedFigure.IsMoving = true;
            this.runStopButton.Text = this.resourceManager.GetString("Stop_Text", this.cultureInfo);
        }

        /// <summary>
        /// Calls after selecting item in TreeView.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of TreeViewFigures select-event.</param>
        private void TreeViewFigures_AfterSelect(object sender, TreeViewEventArgs e)
        {
            this.selectedFigure = null;
            var selectedNodeText = e.Node.Text;

            this.selectedFigure = this.figures.FirstOrDefault(f => f.Name == selectedNodeText);

            if (this.selectedFigure == null)
            {
                return;
            }

            this.runStopButton.Enabled = true;
            this.plusButton.Enabled = true;
            this.minusButton.Enabled = true;

            if (this.selectedFigure.IsMoving)
            {
                this.runStopButton.Text = this.resourceManager.GetString("Stop_Text", this.cultureInfo);
            }

            if (!this.selectedFigure.IsMoving)
            {
                this.runStopButton.Text = this.resourceManager.GetString("Run_Text", this.cultureInfo);
            }
        }

        /// <summary>
        /// Calls after selecting item in LanguageComboBox.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of LanguageComboBox select-event.</param>
        private void LanguageComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedState = this.languageComboBox.SelectedItem.ToString();
            this.CultureInfo = new CultureInfo("en");

            if (selectedState == "Русский")
            {
                this.CultureInfo = new CultureInfo("ru-RU");
            }

            this.SetLabelsText();
        }

        /// <summary>
        /// Set text for all Labels.
        /// </summary>
        private void SetLabelsText()
        {
            this.buttonClear.Text = this.resourceManager.GetString("Clean_Button", this.cultureInfo);
            this.languageLabel.Text = this.resourceManager.GetString("Language_Label_Text", this.cultureInfo);
            this.menuToolStripMenuItem.Text = this.resourceManager.GetString("Menu_Text", this.cultureInfo);
            this.openToolStripMenuItem.Text = this.resourceManager.GetString("Open_Text", this.cultureInfo);
            this.saveToolStripMenuItem.Text = this.resourceManager.GetString("Save_Text", this.cultureInfo);
            this.figureToolStripMenuItem.Text = this.resourceManager.GetString("Figure_Text", this.cultureInfo);
            this.positionToolStripMenuItem.Text = this.resourceManager.GetString("Position_Text", this.cultureInfo);
            this.circleToolStripMenuItem.Text = this.resourceManager.GetString("Circle_Text", this.cultureInfo);
            this.rectangleToolStripMenuItem.Text = this.resourceManager.GetString("Rectangle_Text", this.cultureInfo);
            this.triangleToolStripMenuItem.Text = this.resourceManager.GetString("Triangle_Text", this.cultureInfo);

            if (this.treeViewFigures.Nodes.Count != 0)
            {
                this.treeViewFigures.Nodes[0].Text = this.resourceManager.GetString("Figures_Text", this.cultureInfo);
                this.treeViewFigures.Nodes[0].Nodes[0].Text = this.resourceManager.GetString("Circles_Text", this.cultureInfo);
                this.treeViewFigures.Nodes[0].Nodes[1].Text = this.resourceManager.GetString("Rectangles_Text", this.cultureInfo);
                this.treeViewFigures.Nodes[0].Nodes[2].Text = this.resourceManager.GetString("Triangles_Text", this.cultureInfo);
            }

            if (!this.runStopButton.Enabled)
            {
                this.runStopButton.Text = this.resourceManager.GetString("Run_Text", this.cultureInfo);
            }

            if (this.selectedFigure == null)
            {
                return;
            }

            if (this.selectedFigure.IsMoving)
            {
                this.runStopButton.Text = this.resourceManager.GetString("Stop_Text", this.cultureInfo);
            }

            if (!this.selectedFigure.IsMoving)
            {
                this.runStopButton.Text = this.resourceManager.GetString("Run_Text", this.cultureInfo);
            }
        }

        /// <summary>
        /// Added function to figure.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of PlusButton click-event.</param>
        private void PlusButton_Click(object sender, EventArgs e)
        {
            this.selectedFigure.BisectionFigures += this.CreateSound;
        }

        /// <summary>
        /// Deleted function from figure.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of MinusButton click-event.</param>
        private void MinusButton_Click(object sender, EventArgs e)
        {
            this.selectedFigure.BisectionFigures -= this.CreateSound;
        }

        /// <summary>
        /// Created sound "beep" and writes to console coordinates of bisection.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of bisection event.</param>
        private void CreateSound(object sender, BisectionFiguresEventArgs e)
        {
            Task.Run(() => 
            { 
                Console.Beep();
                Console.WriteLine($"Bisection {e.BisectionFigures[0].Name}-{e.BisectionFigures[1].Name}{e.BisectionCoordinates}");
            });
        }

        /// <summary>
        /// Drawing figure.
        /// </summary>
        /// <param name="obj">Object of drawer.</param>
        private void DrawFigure(object obj)
        {
            lock (locker)
            {
                var drawer = (DrawerModel)obj;
                drawer.Figure.Draw(drawer.Graphics);
            }
        }
    }
}