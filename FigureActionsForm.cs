﻿//-----------------------------------------------------------------------
// <copyright file="FigureActionsForm.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace FigureMovingApp
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Resources;
    using System.Windows.Forms;
    using FigureMovingApp.Common.Extensions;
    using FigureMovingApp.Figures;
    using FigureMovingApp.Services;

    /// <summary>
    /// Form for serialization.
    /// </summary>
    public partial class FigureActionsForm : Form
    {
        /// <summary>
        /// Instance of resource manager.
        /// </summary>
        private readonly ResourceManager resourceManager;

        /// <summary>
        /// Instance of serialization service.
        /// </summary>
        private readonly SerializeService serializeService;

        /// <summary>
        /// List of figures for ListBox.
        /// </summary>
        private readonly List<Figure> figures;

        /// <summary>
        /// Instance of culture info(localization).
        /// </summary>
        private CultureInfo cultureInfo;

        /// <summary>
        /// Figure that need to serializing.
        /// </summary>
        private Figure savedFigure;

        /// <summary>
        /// Point that need to serializing.
        /// </summary>
        private Point savedPoint;

        /// <summary>
        /// Initializes a new instance of the <see cref="FigureActionsForm" /> class.
        /// </summary>
        /// <param name="figures">List of figures for ListBox.</param>
        /// <param name="cultureInfo">Localization of app.</param>
        /// <param name="resourceManager">Resource manager of app.</param>
        public FigureActionsForm(List<Figure> figures, CultureInfo cultureInfo, ResourceManager resourceManager)
        {
            this.resourceManager = resourceManager;
            this.cultureInfo = cultureInfo;
            this.serializeService = new SerializeService();
            this.figures = figures;
            this.InitializeComponent();
            this.SetLabelsText();
            this.figuresListBox.DataSource = figures;
            this.figuresListBox.DisplayMember = "Name";
            this.figuresListBox.ValueMember = "Id";
        }

        /// <summary>
        /// Select serializing object. Calls after selecting figure.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of listBox select-event.</param>
        private void FiguresListBox_Select(object sender, EventArgs e)
        {
            this.saveButton.Enabled = true;
            this.savedFigure = (Figure)this.figuresListBox.SelectedItem;
            this.savedPoint = this.savedFigure.Coordinates;
        }

        /// <summary>
        /// Save serializing object as file.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of saveButton click-event.</param>
        private void SaveButton_Click(object sender, EventArgs e)
        {
            FileStream stream;

            if (this.saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.Close();
            }

            try
            {
                stream = (FileStream)this.saveFileDialog.OpenFile();
            }
            catch 
            {
                return;
            }

            var fileType = this.saveFileDialog.FileName.Split('.')[1];

            if (this.figureRadioButton.Checked)
            {
                this.serializeService.SerializeObject(this.savedFigure, this.GetSerializeFormat(fileType), stream);
            }

            if (this.pointRadioButton.Checked)
            {
                this.serializeService.SerializeObject(this.savedFigure.Coordinates, this.GetSerializeFormat(fileType), stream);
            }
        }

        /// <summary>
        /// Set text for all Labels.
        /// </summary>
        private void SetLabelsText()
        {
            this.figureRadioButton.Text = this.resourceManager.GetString("Figure_Text", this.cultureInfo);
            this.pointRadioButton.Text = this.resourceManager.GetString("Point_Text", this.cultureInfo);
            this.saveButton.Text = this.resourceManager.GetString("Save_Text", this.cultureInfo);
        }
    }
}