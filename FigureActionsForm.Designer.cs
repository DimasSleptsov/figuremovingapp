﻿//-----------------------------------------------------------------------
// <copyright file="FigureActionsForm.Designer.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace FigureMovingApp
{
    /// <summary>
    /// Design part of FigureActionsForm class.
    /// </summary>
    public partial class FigureActionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Top Panel of form.
        /// </summary>
        private System.Windows.Forms.Panel topPanel;

        /// <summary>
        /// Bottom Panel of form.
        /// </summary>
        private System.Windows.Forms.Panel bottomPanel;

        /// <summary>
        /// Button for saving objects as file.
        /// </summary>
        private System.Windows.Forms.Button saveButton;

        /// <summary>
        /// ListBox with figures in main form.
        /// </summary>
        private System.Windows.Forms.ListBox figuresListBox;

        /// <summary>
        /// RadioButton for selecting point serialization.
        /// </summary>
        private System.Windows.Forms.RadioButton pointRadioButton;

        /// <summary>
        /// RadioButton for selecting figure serialization.
        /// </summary>
        private System.Windows.Forms.RadioButton figureRadioButton;

        /// <summary>
        /// Dialog for saving serializing objects.
        /// </summary>
        private System.Windows.Forms.SaveFileDialog saveFileDialog;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.topPanel = new System.Windows.Forms.Panel();
            this.pointRadioButton = new System.Windows.Forms.RadioButton();
            this.figureRadioButton = new System.Windows.Forms.RadioButton();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.saveButton = new System.Windows.Forms.Button();
            this.figuresListBox = new System.Windows.Forms.ListBox();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.topPanel.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.pointRadioButton);
            this.topPanel.Controls.Add(this.figureRadioButton);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(308, 43);
            this.topPanel.TabIndex = 1;
            // 
            // pointRadioButton
            // 
            this.pointRadioButton.AutoSize = true;
            this.pointRadioButton.Location = new System.Drawing.Point(85, 13);
            this.pointRadioButton.Name = "pointRadioButton";
            this.pointRadioButton.Size = new System.Drawing.Size(49, 17);
            this.pointRadioButton.TabIndex = 1;
            this.pointRadioButton.Text = "Point";
            this.pointRadioButton.UseVisualStyleBackColor = true;
            // 
            // figureRadioButton
            // 
            this.figureRadioButton.AutoSize = true;
            this.figureRadioButton.Checked = true;
            this.figureRadioButton.Location = new System.Drawing.Point(13, 13);
            this.figureRadioButton.Name = "figureRadioButton";
            this.figureRadioButton.Size = new System.Drawing.Size(54, 17);
            this.figureRadioButton.TabIndex = 0;
            this.figureRadioButton.TabStop = true;
            this.figureRadioButton.Text = "Figure";
            this.figureRadioButton.UseVisualStyleBackColor = true;
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.saveButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 271);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(308, 36);
            this.bottomPanel.TabIndex = 2;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(221, 6);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 0;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // figuresListBox
            // 
            this.figuresListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.figuresListBox.FormattingEnabled = true;
            this.figuresListBox.Location = new System.Drawing.Point(0, 43);
            this.figuresListBox.Name = "figuresListBox";
            this.figuresListBox.Size = new System.Drawing.Size(308, 228);
            this.figuresListBox.TabIndex = 3;
            this.figuresListBox.SelectedIndexChanged += new System.EventHandler(this.FiguresListBox_Select);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "dat";
            this.saveFileDialog.Filter = "(*.dat)|*.dat|(*.json)|*.json|(*.xml)|*.xml";
            // 
            // FigureActionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 307);
            this.Controls.Add(this.figuresListBox);
            this.Controls.Add(this.bottomPanel);
            this.Controls.Add(this.topPanel);
            this.Name = "FigureActionsForm";
            this.Text = "SaveFigure";
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.bottomPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
    }
}