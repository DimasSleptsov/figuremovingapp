﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------

namespace FigureMovingApp
{
    using System;
    using System.Windows.Forms;

    /// <summary>
    /// Program class of app.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FigureMovingAppMainForm());
        }
    }
}