﻿//-----------------------------------------------------------------------
// <copyright file="SelectPositionForFigureForm.Designer.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------

namespace FigureMovingApp
{
    /// <summary>
    /// Design part of SelectPositionForFigureForm class.
    /// </summary>
    public partial class SelectPositionForFigureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Top Panel of form.
        /// </summary>
        private System.Windows.Forms.Panel topPanel;

        /// <summary>
        /// Bottom Panel of form.
        /// </summary>
        private System.Windows.Forms.Panel bottomPanel;

        /// <summary>
        /// Label with action advice.
        /// </summary>
        private System.Windows.Forms.Label selectFigureLabel;

        /// <summary>
        /// Button for changing figure.
        /// </summary>
        private System.Windows.Forms.Button applyButton;

        /// <summary>
        /// ListBox with figures in main form.
        /// </summary>
        private System.Windows.Forms.ListBox figuresListBox;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.topPanel = new System.Windows.Forms.Panel();
            this.selectFigureLabel = new System.Windows.Forms.Label();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.applyButton = new System.Windows.Forms.Button();
            this.figuresListBox = new System.Windows.Forms.ListBox();
            this.topPanel.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.selectFigureLabel);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(295, 41);
            this.topPanel.TabIndex = 1;
            // 
            // selectFigureLabel
            // 
            this.selectFigureLabel.AutoSize = true;
            this.selectFigureLabel.Location = new System.Drawing.Point(13, 13);
            this.selectFigureLabel.Name = "selectFigureLabel";
            this.selectFigureLabel.Size = new System.Drawing.Size(69, 13);
            this.selectFigureLabel.TabIndex = 0;
            this.selectFigureLabel.Text = "Select Figure";
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.applyButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 204);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(295, 46);
            this.bottomPanel.TabIndex = 2;
            // 
            // applyButton
            // 
            this.applyButton.Enabled = false;
            this.applyButton.Location = new System.Drawing.Point(208, 11);
            this.applyButton.Name = "applyButton";
            this.applyButton.Size = new System.Drawing.Size(75, 23);
            this.applyButton.TabIndex = 0;
            this.applyButton.Text = "Apply";
            this.applyButton.UseVisualStyleBackColor = true;
            this.applyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // figuresListBox
            // 
            this.figuresListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.figuresListBox.FormattingEnabled = true;
            this.figuresListBox.Location = new System.Drawing.Point(0, 41);
            this.figuresListBox.Name = "figuresListBox";
            this.figuresListBox.Size = new System.Drawing.Size(295, 163);
            this.figuresListBox.TabIndex = 3;
            this.figuresListBox.SelectedIndexChanged += new System.EventHandler(this.FiguresListBox_SelectedIndexChanged);
            // 
            // SelectPositionForFigureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 250);
            this.Controls.Add(this.figuresListBox);
            this.Controls.Add(this.bottomPanel);
            this.Controls.Add(this.topPanel);
            this.Name = "SelectPositionForFigureForm";
            this.Text = "SelectPositionForFigureForm";
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.bottomPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
    }
}