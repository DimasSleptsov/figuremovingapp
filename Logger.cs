﻿//-----------------------------------------------------------------------
// <copyright file="Logger.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------

namespace FigureMovingApp
{
    using log4net;
    using log4net.Config;

    /// <summary>
    /// Class for initialization logger.
    /// </summary>
    public static class Logger
    {
        /// <summary>
        /// Value of logger.
        /// </summary>
        private static ILog log = LogManager.GetLogger("LOGGER");

        /// <summary>
        /// Gets log object.
        /// </summary>
        public static ILog Log
        {
            get { return log; }
        }

        /// <summary>
        /// Initialize logger.
        /// </summary>
        public static void InitLogger()
        {
            XmlConfigurator.Configure();
        }
    }
}
