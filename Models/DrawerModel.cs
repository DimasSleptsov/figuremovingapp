﻿//-----------------------------------------------------------------------
// <copyright file="DrawerModel.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------

namespace FigureMovingApp.Models
{
    using System.Drawing;
    using FigureMovingApp.Figures;

    /// <summary>
    /// Drawer model for threads.
    /// </summary>
    public class DrawerModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DrawerModel" /> class.
        /// </summary>
        /// <param name="figure">Figure that need to draw.</param>
        /// <param name="graphics">Graphics of PictureBox.</param>
        public DrawerModel(Figure figure, Graphics graphics)
        {
            this.Figure = figure;
            this.Graphics = graphics;
        }

        /// <summary>
        /// Gets or sets figure.
        /// </summary>
        public Figure Figure { get; set; }

        /// <summary>
        /// Gets or sets graphics of PictureBox.
        /// </summary>
        public Graphics Graphics { get; set; }
    }
}
