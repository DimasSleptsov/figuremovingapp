﻿//-----------------------------------------------------------------------
// <copyright file="SelectPositionForFigureForm.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------

namespace FigureMovingApp
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Resources;
    using System.Windows.Forms;
    using FigureMovingApp.Figures;

    /// <summary>
    /// Class of form that need to select position for figure.
    /// </summary>
    public partial class SelectPositionForFigureForm : Form
    {
        /// <summary>
        /// Instance of resource manager.
        /// </summary>
        private readonly ResourceManager resourceManager;

        /// <summary>
        /// List of figures in main form.
        /// </summary>
        private readonly List<Figure> figures;

        /// <summary>
        /// Instance of culture info(localization).
        /// </summary>
        private CultureInfo cultureInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectPositionForFigureForm" /> class.
        /// </summary>
        /// <param name="figures">List of figures for ListBox.</param>
        /// <param name="cultureInfo">Localization of app.</param>
        /// <param name="resourceManager">Resource manager of app.</param>
        public SelectPositionForFigureForm(List<Figure> figures, CultureInfo cultureInfo, ResourceManager resourceManager)
        {
            this.resourceManager = resourceManager;
            this.cultureInfo = cultureInfo;
            this.figures = figures;
            this.InitializeComponent();
            this.SetLabelsText();
            this.figuresListBox.DataSource = figures;
            this.figuresListBox.DisplayMember = "Name";
            this.figuresListBox.ValueMember = "Id";
        }

        /// <summary>
        /// Gets or sets alterable figure.
        /// </summary>
        public Figure AlterableFigure { get; set; }

        /// <summary>
        /// Select Alterable Figure. Calls after selecting figure.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of listBox select-event.</param>
        private void FiguresListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.applyButton.Enabled = true;
            this.AlterableFigure = (Figure)this.figuresListBox.SelectedItem;
        }

        /// <summary>
        /// Set position for figure.
        /// </summary>
        /// <param name="sender">Object of sender of event.</param>
        /// <param name="e">Event args of applyButton click-event.</param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// Set text for all Labels.
        /// </summary>
        private void SetLabelsText()
        {
            this.selectFigureLabel.Text = this.resourceManager.GetString("Select_Figure", this.cultureInfo);
            this.applyButton.Text = this.resourceManager.GetString("Apply_Button", this.cultureInfo);
        }
    }   
}