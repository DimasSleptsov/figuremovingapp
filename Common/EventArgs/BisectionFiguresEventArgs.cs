﻿//-----------------------------------------------------------------------
// <copyright file="BisectionFiguresEventArgs.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------

namespace FigureMovingApp.Common.EventArgs
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using FigureMovingApp.Figures;

    /// <summary>
    /// Class of args of Bisection event.
    /// </summary>
    public class BisectionFiguresEventArgs : EventArgs
    {
        /// <summary>
        /// Coordinates of bisection.
        /// </summary>
        private readonly Point bisectionCoordinates;

        /// <summary>
        /// Figures that is bisection.
        /// </summary>
        private readonly List<Figure> bisectionFigures;

        /// <summary>
        /// Initializes a new instance of the <see cref="BisectionFiguresEventArgs" /> class.
        /// </summary>
        /// <param name="figures">Figures that is bisection.</param>
        /// <param name="coordinates">Coordinates of bisection.</param>
        public BisectionFiguresEventArgs(List<Figure> figures, Point coordinates)
        {
            this.bisectionCoordinates = coordinates;
            this.bisectionFigures = figures;
        }

        /// <summary>
        /// Gets coordinates of bisection.
        /// </summary>
        public Point BisectionCoordinates 
        { 
            get { return this.bisectionCoordinates; } 
        }

        /// <summary>
        /// Gets figures of bisection.
        /// </summary>
        public List<Figure> BisectionFigures 
        {
            get { return this.bisectionFigures; } 
        }
    }
}