﻿//-----------------------------------------------------------------------
// <copyright file="SerializeExtension.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------

namespace FigureMovingApp.Common.Extensions
{
    using System.Windows.Forms;
    using FigureMovingApp.Common.Enums;

    /// <summary>
    /// Serialization Extension for forms class.
    /// </summary>
    public static class SerializeExtension
    {
        /// <summary>
        /// Extension method for calculating serializing format.
        /// </summary>
        /// <param name="control">Instance of control, where needed calculate serialization format.</param>
        /// <param name="fileType">Type of serialization file.</param>
        /// <returns>Serialization format of file.</returns>
        public static SerializeFormat GetSerializeFormat(this Control control, string fileType)
        {
            if (fileType == "dat")
            {
                return SerializeFormat.Bin;
            }

            if (fileType == "json")
            {
                return SerializeFormat.Json;
            }

            return SerializeFormat.XML;
        }
    }
}