﻿//-----------------------------------------------------------------------
// <copyright file="SerializeFormat.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------

namespace FigureMovingApp.Common.Enums
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Enum of format of serialization file.
    /// </summary>
    public enum SerializeFormat
    {
        /// <summary>
        /// Binary(".dat") format of serialization file.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed.")]
        Bin = 0,

        /// <summary>
        /// Json format of serialization file.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed.")]
        Json = 1,

        /// <summary>
        /// Xml format of serialization file.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed.")]
        XML = 2
    }
}
