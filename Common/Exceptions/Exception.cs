﻿//-----------------------------------------------------------------------
// <copyright file="Exception.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace FigureMovingApp.Common.Exceptions
{
    using System;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    /// <summary>
    /// Generic Exception class.
    /// </summary>
    /// <typeparam name="TExceptionArgs">Args of Exception</typeparam>
    [Serializable]
    public sealed class Exception<TExceptionArgs> : Exception, ISerializable
        where TExceptionArgs : ExceptionArgs
    {
        /// <summary>
        /// String of Exception Args.
        /// </summary>
        private const string ExceptianArgsString = "Args";

        /// <summary>
        /// Value of Exception Args.
        /// </summary>
        private readonly TExceptionArgs exceptionArgs;

        /// <summary>
        /// Initializes a new instance of the <see cref="Exception{TExceptionArgs}" /> class.
        /// </summary>
        /// <param name="message">message of Exception</param>
        /// <param name="innerException">Value of inner exception.</param>
        public Exception(string message = null, Exception innerException = null)
            : this(null, message, innerException)
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Exception{TExceptionArgs}" /> class.
        /// </summary>
        /// <param name="args">Args of Exception</param>
        /// <param name="message">message of Exception</param>
        /// <param name="innerException">Value of inner exception.</param>
        public Exception(TExceptionArgs args, string message = null, Exception innerException = null) : base(message, innerException)
        {
            this.exceptionArgs = args;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Exception{TExceptionArgs}" /> class.
        /// </summary>
        /// <param name="info">Exception information</param>
        /// <param name="context">Context of exception</param>
        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        private Exception(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this.exceptionArgs = (TExceptionArgs)info.GetValue(
                ExceptianArgsString, typeof(TExceptionArgs));
        }

        /// <summary>
        /// Gets exceptionArgs.
        /// </summary>
        public TExceptionArgs Args
        {
            get
            {
                return this.exceptionArgs;
            }
        }

        /// <summary>
        /// Gets exception message.
        /// </summary>
        public override string Message
        {
            get
            {
                string baseMsg = base.Message;
                return (this.exceptionArgs == null) ? baseMsg : baseMsg + " (" + this.exceptionArgs.Message + ")";
            }
        }

        /// <summary>
        /// Gets ObjectData of exception.
        /// </summary>
        /// <param name="info">Exception information.</param>
        /// <param name="context">Context of exception.</param>
        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(ExceptianArgsString, this.exceptionArgs);
            base.GetObjectData(info, context);
        }

        /// <summary>
        /// Gets ObjectData of exception.
        /// </summary>
        /// <param name="obj">Value of Exception.</param>
        /// <returns>Is Equals args</returns>
        public override bool Equals(object obj)
        {
            Exception<TExceptionArgs> other = obj as Exception<TExceptionArgs>;
            if (obj == null)
            { 
                return false; 
            }

            return object.Equals(this.exceptionArgs, other.exceptionArgs) && base.Equals(obj);
        }

        /// <summary>
        /// Gets HashCode of exception.
        /// </summary>
        /// <returns>HashCode of exception.</returns>
        public override int GetHashCode() 
        {
            return base.GetHashCode(); 
        }
    }
}
