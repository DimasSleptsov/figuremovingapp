﻿//-----------------------------------------------------------------------
// <copyright file="ExceptionArgs.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace FigureMovingApp.Common.Exceptions
{
    using System;

    /// <summary>
    /// ExceptionArgs class.
    /// </summary>
    [Serializable]
    public abstract class ExceptionArgs
    {
        /// <summary>
        /// Gets exception message.
        /// </summary>
        public virtual string Message
        {
            get
            {
                return string.Empty;
            }
        }
    }
}
