﻿//-----------------------------------------------------------------------
// <copyright file="OutOfAreaExceptionArgs.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace FigureMovingApp.Common.Exceptions
{
    using System;
    using FigureMovingApp.Figures;

    /// <summary>
    /// OutOfAreaExceptionArgs class.
    /// </summary>
    [Serializable]
    public sealed class OutOfAreaExceptionArgs : ExceptionArgs
    {
        /// <summary>
        /// Figure that out of area.
        /// </summary>
        private readonly Figure figureOutOfArea;

        /// <summary>
        /// Initializes a new instance of the <see cref="OutOfAreaExceptionArgs" /> class.
        /// </summary>
        /// <param name="figure">Out of area figure.</param>
        public OutOfAreaExceptionArgs(Figure figure) 
        { 
            this.figureOutOfArea = figure; 
        }

        /// <summary>
        /// Gets out of area coordinates.
        /// </summary>
        public Figure Figure
        { 
            get 
            { 
                return this.figureOutOfArea; 
            } 
        }

        /// <summary>
        /// Gets exception message.
        /// </summary>
        public override string Message
        {
            get
            {
                return (this.figureOutOfArea == null) ? base.Message 
                    : $"Figure {this.figureOutOfArea.Name} is out of PictureBox in point{this.figureOutOfArea.Coordinates}";
            }
        }
    }
}