﻿//-----------------------------------------------------------------------
// <copyright file="Triangle.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace FigureMovingApp.Figures
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    /// <summary>
    /// Triangle figure class.
    /// </summary>
    [Serializable]
    public class Triangle : Figure
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Triangle" /> class.
        /// </summary>
        /// <param name="pictureBox">PictureBox instance, where would be draw triangle</param>
        public Triangle(PictureBox pictureBox) : base(pictureBox)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Triangle" /> class.
        /// </summary>
        public Triangle()
        {
        }

        /// <summary>
        /// Gets or sets A-point of triangle.
        /// </summary>
        public Point APoint { get; set; }

        /// <summary>
        /// Gets or sets B-point of triangle.
        /// </summary>
        public Point BPoint { get; set; }

        /// <summary>
        /// Gets or sets C-point of triangle.
        /// </summary>
        public Point CPoint { get; set; }

        /// <summary>
        /// Draw triangle by graphics-instance of control of windows forms
        /// </summary>
        /// <param name="graphics">Graphics instance of control. Drawing surface.</param>
        public override void Draw(Graphics graphics)
        {
            this.APoint = new Point(this.Coordinates.X + 15, this.Coordinates.Y);
            this.BPoint = new Point(this.Coordinates.X, this.Coordinates.Y + 30);
            this.CPoint = new Point(this.Coordinates.X + 30, this.Coordinates.Y + 30);

            graphics.DrawLine(new Pen(Color.Black), this.APoint, this.BPoint);
            graphics.DrawLine(new Pen(Color.Black), this.APoint, this.CPoint);
            graphics.DrawLine(new Pen(Color.Black), this.BPoint, this.CPoint);
        }

        /// <summary>
        /// Draw triangle by graphics-instance of control of windows forms
        /// </summary>
        /// <param name="graphics">Graphics instance of control. Drawing surface.</param>
        /// <param name="button">Button instance, where would be draw triangle.</param>
        public override void Draw(Graphics graphics, Button button)
        {
            this.APoint = new Point(button.Width / 2, (button.Height / 2) - 15);
            this.BPoint = new Point((button.Width / 2) - 15, (button.Height / 2) + 15);
            this.CPoint = new Point((button.Width / 2) + 15, (button.Height / 2) + 15);

            graphics.DrawLine(new Pen(Color.Black), this.APoint, this.BPoint);
            graphics.DrawLine(new Pen(Color.Black), this.APoint, this.CPoint);
            graphics.DrawLine(new Pen(Color.Black), this.BPoint, this.CPoint);
        }

        /// <summary>
        /// Gets bisection figure and coordinates of bisection.
        /// </summary>
        /// <param name="figures">List of figures of same type.</param>
        /// <returns>Bisection Figure and coordinates of bisection.</returns>
        protected override (Figure, Point) GetBisectionFigureAndPoint(List<Figure> figures)
        {
            figures.Remove(this);

            if (figures.Count == 0)
            {
                return (null, this.Coordinates);
            }

            foreach (Triangle f in figures)
            {
                List<Point> points = new List<Point>() { f.APoint, f.BPoint, f.CPoint };

                foreach (Point p in points)
                {
                    if (this.GetBisection(this, p))
                    {
                        if (this.IsBisectionNow && this.BisectionFiguresList.FirstOrDefault(x => x.Id == f.Id) != null)
                        {
                            return (null, this.Coordinates);
                        }

                        this.BisectionFiguresList.Add(f);
                        this.IsBisectionNow = true;
                        return (f, p);
                    }
                }

                points = new List<Point>() { this.APoint, this.BPoint, this.CPoint };

                foreach (Point p in points)
                {
                    if (this.GetBisection(f, p))
                    {
                        if (this.IsBisectionNow && this.BisectionFiguresList.FirstOrDefault(x => x.Id == f.Id) != null)
                        {
                            return (null, this.Coordinates);
                        }

                        this.BisectionFiguresList.Add(f);
                        this.IsBisectionNow = true;
                        return (f, p);
                    }
                }
            }

            this.BisectionFiguresList.Clear();
            this.IsBisectionNow = false;
            return (null, this.Coordinates);
        }

        /// <summary>
        /// Gets Out Coordinates of Triangle.
        /// </summary>
        /// <returns>Out Point.</returns>
        protected override Point GetOutCoordinates()
        {
            return this.CPoint;
        }

        /// <summary>
        /// Gets true, if bisection figure.
        /// </summary>
        /// <param name="triangle">Triangle of bisection.</param>
        /// <param name="point">Point of bisection.</param>
        /// <returns>True, if bisection figure</returns>
        private bool GetBisection(Triangle triangle, Point point)
        {
            if (triangle.APoint.X == 0 && triangle.APoint.Y == 0 && 
                triangle.BPoint.X == 0 && triangle.BPoint.Y == 0 &&
                triangle.CPoint.X == 0 && triangle.CPoint.Y == 0)
            {
                return false;
            }

            var pointRegardingLeftSide = ((triangle.APoint.X - point.X) * (triangle.BPoint.Y - triangle.APoint.Y)) -
                ((triangle.BPoint.X - triangle.APoint.X) * (triangle.APoint.Y - point.Y));
            var pointRegardingRightSide = ((triangle.CPoint.X - point.X) * (triangle.APoint.Y - triangle.CPoint.Y)) -
                ((triangle.APoint.X - triangle.CPoint.X) * (triangle.CPoint.Y - point.Y));
            var pointRegardingDownSide = ((triangle.BPoint.X - point.X) * (triangle.CPoint.Y - triangle.BPoint.Y)) -
                ((triangle.CPoint.X - triangle.BPoint.X) * (triangle.BPoint.Y - point.Y));
            return (pointRegardingDownSide >= 0 && pointRegardingLeftSide >= 0 && pointRegardingRightSide >= 0) ||
                (pointRegardingDownSide <= 0 && pointRegardingLeftSide <= 0 && pointRegardingRightSide <= 0);
        }
    }
}