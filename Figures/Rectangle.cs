﻿//-----------------------------------------------------------------------
// <copyright file="Rectangle.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------

namespace FigureMovingApp.Figures
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    /// <summary>
    /// Rectangle figure class.
    /// </summary>
    [Serializable]
    public class Rectangle : Figure
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Rectangle" /> class.
        /// </summary>
        /// <param name="pictureBox">PictureBox instance, where would be draw rectangle</param>
        public Rectangle(PictureBox pictureBox) : base(pictureBox)
        {
            this.APoint = new Point(this.Coordinates.X, this.Coordinates.Y);
            this.BPoint = new Point(this.Coordinates.X + 30, this.Coordinates.Y);
            this.CPoint = new Point(this.Coordinates.X, this.Coordinates.Y + 30);
            this.DPoint = new Point(this.Coordinates.X + 30, this.Coordinates.Y + 30);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Rectangle" /> class.
        /// </summary>
        public Rectangle()
        {
        }

        /// <summary>
        /// Gets or sets A-point of rectangle.
        /// </summary>
        public Point APoint { get; set; }

        /// <summary>
        /// Gets or sets B-point of rectangle.
        /// </summary>
        public Point BPoint { get; set; }

        /// <summary>
        /// Gets or sets C-point of rectangle.
        /// </summary>
        public Point CPoint { get; set; }

        /// <summary>
        /// Gets or sets D-point of rectangle.
        /// </summary>
        public Point DPoint { get; set; }

        /// <summary>
        /// Draw rectangle by graphics-instance of control of windows forms
        /// </summary>
        /// <param name="graphics">Graphics instance of control. Drawing surface.</param>
        public override void Draw(Graphics graphics)
        {
            this.APoint = new Point(this.Coordinates.X, this.Coordinates.Y);
            this.BPoint = new Point(this.Coordinates.X + 30, this.Coordinates.Y);
            this.CPoint = new Point(this.Coordinates.X, this.Coordinates.Y + 30);
            this.DPoint = new Point(this.Coordinates.X + 30, this.Coordinates.Y + 30);
            graphics.DrawRectangle(new Pen(Color.Green), Coordinates.X, Coordinates.Y, 30, 30);
        }

        /// <summary>
        /// Draw rectangle by graphics-instance of control of windows forms
        /// </summary>
        /// <param name="graphics">Graphics instance of control. Drawing surface.</param>
        /// <param name="button">Button instance, where would be draw rectangle.</param>
        public override void Draw(Graphics graphics, Button button)
        {
            graphics.DrawRectangle(new Pen(Color.Green), (button.Width / 2) - 15, (button.Height / 2) - 15, 30, 30);
        }

        /// <summary>
        /// Gets bisection figure and coordinates of bisection.
        /// </summary>
        /// <param name="figures">List of figures of same type.</param>
        /// <returns>Bisection Figure and coordinates of bisection.</returns>
        protected override (Figure, Point) GetBisectionFigureAndPoint(List<Figure> figures)
        {
            figures.Remove(this);

            if (figures.Count == 0)
            {
                return (null, this.Coordinates);
            }

            foreach (Rectangle f in figures)
            {
                List<Point> points = new List<Point>() { f.APoint, f.BPoint, f.CPoint, f.DPoint };
                var bisectionPoint = points.FirstOrDefault(p =>
                     ((p.X >= this.CPoint.X) && (p.X <= this.DPoint.X) &&
                     ((p.Y >= this.CPoint.Y) && (p.Y <= this.APoint.Y))));
                if (bisectionPoint != null)
                {
                    if (this.IsBisectionNow && this.BisectionFiguresList.FirstOrDefault(x => x.Id == f.Id) != null)
                    {
                        return (null, this.Coordinates);
                    }

                    this.BisectionFiguresList.Add(f);
                    this.IsBisectionNow = true;
                    return (f, bisectionPoint);
                }
            }

            this.BisectionFiguresList.Clear();
            this.IsBisectionNow = false;
            return (null, this.Coordinates);
        }

        /// <summary>
        /// Gets Out Coordinates of Rectangle.
        /// </summary>
        /// <returns>Out Point.</returns>
        protected override Point GetOutCoordinates()
        {
            return this.DPoint;
        }
    }
}