﻿//-----------------------------------------------------------------------
// <copyright file="Figure.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace FigureMovingApp.Figures
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Threading;
    using System.Windows.Forms;
    using FigureMovingApp.Common.EventArgs;
    using FigureMovingApp.Common.Exceptions;
    using FigureMovingAppHelpers;

    /// <summary>
    /// Abstract class of figure that inherit all figures.
    /// </summary>
    [Serializable]
    public abstract class Figure
    {
        /// <summary>
        /// RandomHelper variable for generating random variables.
        /// </summary>
        [NonSerialized]
        private readonly RandomHelper randomHelper;

        /// <summary>
        /// All speeds of figure.
        /// </summary>
        [NonSerialized]
        private readonly int[] speeds = new int[8] { -4, -3, -2, -1, 1, 2, 3, 4 };

        /// <summary>
        /// PictureBox variable.
        /// </summary>
        [NonSerialized]
        private PictureBox pictureBox;

        /// <summary>
        /// Initializes a new instance of the <see cref="Figure" /> class.
        /// </summary>
        /// <param name="pictureBox">PictureBox instance, where would be draw circle</param>
        public Figure(PictureBox pictureBox)
        {
            this.pictureBox = pictureBox;
            this.Id = Guid.NewGuid().ToString();
            this.randomHelper = new RandomHelper();
            Point maxPoint = new Point(this.pictureBox.Width - 30, this.pictureBox.Height - 30);
            this.Coordinates = this.randomHelper.GetRandomPoint(maxPoint);
            (this.Dx, this.Dy) = this.randomHelper.GetRandomSpeedByArray(this.speeds);
            this.BisectionFiguresList = new List<Figure>();
            this.IsMoving = true;
        }

        /// <summary>        
        /// Initializes a new instance of the <see cref="Figure" /> class.
        /// </summary>
        public Figure()
        {
        }

        /// <summary>        
        /// Event of bisection of figures.
        /// </summary>
        public event EventHandler<BisectionFiguresEventArgs> BisectionFigures;

        /// <summary>
        /// Gets or sets Coordinates of figure.
        /// </summary>
        public Point Coordinates { get; set; }

        /// <summary>
        /// Gets or sets X-speed of figure.
        /// </summary>
        public int Dx { get; set; }

        /// <summary>
        /// Gets or sets Y-speed of figure.
        /// </summary>
        public int Dy { get; set; }

        /// <summary>
        /// Gets or sets name of figure.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets id of figure.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether figure is moving.
        /// </summary>
        public bool IsMoving { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether figure is bisection.
        /// </summary>
        protected bool IsBisectionNow { get; set; }

        /// <summary>
        /// Gets or sets list of figure that bisection with current figure.
        /// </summary>
        protected List<Figure> BisectionFiguresList { get; set; }

        /// <summary>
        /// Uses to move figure.
        /// </summary>
        /// <param name="figures">List of figures of same type.</param>
        public virtual void Move(List<Figure> figures)
        {
            (var bisectionFigure, var bisectionCoordinates) = this.GetBisectionFigureAndPoint(figures.Where(f => f.GetType() == this.GetType()).ToList());
            if (bisectionFigure != null)
            {
                var bisectionFigures = new List<Figure>();
                bisectionFigures.Add(this);
                bisectionFigures.Add(bisectionFigure);
                this.SimulateBisectionFigures(bisectionFigures, bisectionCoordinates);
            }

            if (!this.IsMoving)
            {
                return;
            }

            this.Dx = this.GetSpeedDirection(this.Dx, this.Coordinates.X, this.pictureBox.Width);
            this.Dy = this.GetSpeedDirection(this.Dy, this.Coordinates.Y, this.pictureBox.Height);

            if (!this.CheckResidualSpeed())
            {
                this.Coordinates = new Point(this.Coordinates.X + this.Dx, this.Coordinates.Y + this.Dy);
            }

            var outCoordinates = this.GetOutCoordinates();
            if (outCoordinates.X > this.pictureBox.Width || outCoordinates.Y > this.pictureBox.Height)
            {
                throw new Exception<OutOfAreaExceptionArgs>(new OutOfAreaExceptionArgs(this));
            }
        }

        /// <summary>
        /// Set PictureBox for figure.
        /// </summary>
        /// <param name="pictureBox">Picture box where view figure.</param>
        public void SetPictureBox(PictureBox pictureBox)
        {
            this.pictureBox = pictureBox;
        }

        /// <summary>
        /// Draw figure by graphics-instance of control of windows forms.
        /// </summary>
        /// <param name="graphics">Graphics instance of control. Drawing surface.</param>
        public abstract void Draw(Graphics graphics);

        /// <summary>
        /// Draw figure by graphics-instance of control of windows forms
        /// </summary>
        /// <param name="graphics">Graphics instance of control. Drawing surface.</param>
        /// <param name="button">Button instance, where would be draw circle.</param>
        public abstract void Draw(Graphics graphics, Button button);

        /// <summary>
        /// Simulate BisectionFigure event.
        /// </summary>
        /// <param name="figures">List of bisection figures.</param>
        /// <param name="coordinates">Coordinates of bisection.</param>
        public void SimulateBisectionFigures(List<Figure> figures, Point coordinates)
        {
            BisectionFiguresEventArgs e = new BisectionFiguresEventArgs(figures, coordinates);
            this.OnBisectionFigures(e);
        }

        /// <summary>
        /// Creates event handler for bisection event.
        /// </summary>
        /// <param name="e">Args of BisectionEvent.</param>
        protected virtual void OnBisectionFigures(BisectionFiguresEventArgs e)
        {
            EventHandler<BisectionFiguresEventArgs> eventHandler = Volatile.Read(ref this.BisectionFigures);
            if (eventHandler != null)
            {
                eventHandler(this, e);
            }
        }

        /// <summary>
        /// Gets bisection figure and coordinates of bisection.
        /// </summary>
        /// <param name="figures">List of figures of same type.</param>
        /// <returns>Bisection Figure and coordinates of bisection.</returns>
        protected abstract (Figure, Point) GetBisectionFigureAndPoint(List<Figure> figures);

        /// <summary>
        /// Gets Out Coordinates of Figure.
        /// </summary>
        /// <returns>Out Point.</returns>
        protected abstract Point GetOutCoordinates();

        /// <summary>
        /// Gets direction of speed.
        /// </summary>
        /// <param name="speed">Speed of figure.</param>
        /// <param name="coordinate">X- or Y-coordinate of figure.</param>
        /// <param name="parameter">Value of parameter(width or height) of picture box.</param>
        /// <returns>Figure speed.</returns>
        private int GetSpeedDirection(int speed, int coordinate, int parameter)
        {
            if (coordinate <= 0)
            {
                return Math.Abs(speed);
            }

            if (coordinate >= parameter - 30)
            {
                return Math.Abs(speed) * (-1);
            }

            return speed;
        }

        /// <summary>
        /// Checks that figure have residual speed.
        /// </summary>
        /// <returns>Have residual speed.</returns>
        private bool CheckResidualSpeed()
        {
            bool isResidualSpeed = false;
            if (this.Coordinates.X + this.Dx > this.pictureBox.Width - 30)
            {
                this.Coordinates = new Point(this.pictureBox.Width - 30, this.Coordinates.Y);
                isResidualSpeed = true;
            }

            if (this.Coordinates.Y + this.Dy > this.pictureBox.Height - 30)
            {
                this.Coordinates = new Point(this.Coordinates.X, this.pictureBox.Height - 30);
                isResidualSpeed = true;
            }

            return isResidualSpeed;
        }
    }
}