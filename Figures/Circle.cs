﻿//-----------------------------------------------------------------------
// <copyright file="Circle.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace FigureMovingApp.Figures
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Threading;
    using System.Windows.Forms;

    /// <summary>
    /// Circle figure class.
    /// </summary>
    [Serializable]
    public class Circle : Figure
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Circle" /> class.
        /// </summary>
        /// <param name="pictureBox">PictureBox instance, where would be draw circle</param>
        public Circle(PictureBox pictureBox) : base(pictureBox)
        {
            this.OutPoint = new Point(this.Coordinates.X + 30, this.Coordinates.Y + 30);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Circle" /> class.
        /// </summary>
        public Circle()
        {
        }

        /// <summary>
        /// Gets or sets out point of circle.
        /// </summary>
        public Point OutPoint { get; set; }

        /// <summary>
        /// Draw circle by graphics-instance of control of windows forms
        /// </summary>
        /// <param name="graphics">Graphics instance of control. Drawing surface.</param>
        public override void Draw(Graphics graphics)
        {
            this.OutPoint = new Point(this.Coordinates.X + 30, this.Coordinates.Y + 30);
            graphics.DrawEllipse(new Pen(Color.Red), Coordinates.X, Coordinates.Y, 30, 30);
        }

        /// <summary>
        /// Draw circle by graphics-instance of control of windows forms
        /// </summary>
        /// <param name="graphics">Graphics instance of control. Drawing surface.</param>
        /// <param name="button">Button instance, where would be draw circle.</param>
        public override void Draw(Graphics graphics, Button button)
        {
            graphics.DrawEllipse(new Pen(Color.Red), (button.Width / 2) - 15, (button.Height / 2) - 15, 30, 30);
        }

        /// <summary>
        /// Gets bisection figure and coordinates of bisection.
        /// </summary>
        /// <param name="figures">List of figures of same type.</param>
        /// <returns>Bisection Figure and coordinates of bisection.</returns>
        protected override (Figure, Point) GetBisectionFigureAndPoint(List<Figure> figures)
        {
            figures.Remove(this);
            if (figures.Count == 0)
            {
                return (null, this.Coordinates);
            }

            foreach (Circle f in figures)
            {
                if (Math.Sqrt(Math.Pow(this.Coordinates.X - f.Coordinates.X, 2)
                    + Math.Pow(this.Coordinates.Y - f.Coordinates.Y, 2)) < 30)
                {
                    if (this.IsBisectionNow && this.BisectionFiguresList.FirstOrDefault(x => x.Id == f.Id) != null)
                    {
                        return (null, this.Coordinates);
                    }

                    this.BisectionFiguresList.Add(f);
                    this.IsBisectionNow = true;
                    return (f, this.GetBisectionPoint(this, f));
                }
            }

            this.BisectionFiguresList.Clear();
            this.IsBisectionNow = false;
            return (null, this.Coordinates);
        }

        /// <summary>
        /// Gets Out Coordinates of Circle.
        /// </summary>
        /// <returns>Out Point.</returns>
        protected override Point GetOutCoordinates()
        {
            return this.OutPoint;
        }

        /// <summary>
        /// Gets coordinates of bisection.
        /// </summary>
        /// <param name="firstCircle">First figure of bisection.</param>
        /// <param name="secondCircle">Second figure of bisection.</param>
        /// <returns>Coordinates of bisection.</returns>
        private Point GetBisectionPoint(Circle firstCircle, Circle secondCircle)
        {
            var x = (firstCircle.Coordinates.X + secondCircle.Coordinates.X) / 2;
            var y = (firstCircle.Coordinates.Y + secondCircle.Coordinates.Y) / 2;
            return new Point(x, y);
        }
    }
}