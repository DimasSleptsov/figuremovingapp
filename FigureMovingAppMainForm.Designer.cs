﻿//-----------------------------------------------------------------------
// <copyright file="FigureMovingAppMainForm.Designer.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------

namespace FigureMovingApp
{
    /// <summary>
    /// Design part of FigureMovingApp class.
    /// </summary>
    public partial class FigureMovingAppMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Variable of panel in form.
        /// </summary>
        private System.Windows.Forms.Panel panelTools;

        /// <summary>
        /// Variable of TreeView in form.
        /// </summary>
        private System.Windows.Forms.TreeView treeViewFigures;

        /// <summary>
        /// Variable of Button in form.
        /// </summary>
        private System.Windows.Forms.Button buttonCreateRectangle;

        /// <summary>
        /// Variable of Button in form.
        /// </summary>
        private System.Windows.Forms.Button buttonCreateCircle;

        /// <summary>
        /// Variable of Button in form.
        /// </summary>
        private System.Windows.Forms.Button buttonCreateTriangle;

        /// <summary>
        /// Variable of PictureBox in form.
        /// </summary>
        private System.Windows.Forms.PictureBox pictureBoxMain;

        /// <summary>
        /// Variable of Button in form.
        /// </summary>
        private System.Windows.Forms.Button buttonClear;

        /// <summary>
        /// Variable of Timer in form.
        /// </summary>
        private System.Windows.Forms.Timer timer;

        /// <summary>
        /// MenuStrip of form.
        /// </summary>
        private System.Windows.Forms.MenuStrip menuStripMain;

        /// <summary>
        /// Menu item of MenuStrip of form.
        /// </summary>
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;

        /// <summary>
        /// Open item of MenuStrip of form.
        /// </summary>
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;

        /// <summary>
        /// Save item of MenuStrip of form.
        /// </summary>
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;

        /// <summary>
        /// Figure item of MenuStrip of form.
        /// </summary>
        private System.Windows.Forms.ToolStripMenuItem figureToolStripMenuItem;

        /// <summary>
        /// Position item of MenuStrip of form.
        /// </summary>
        private System.Windows.Forms.ToolStripMenuItem positionToolStripMenuItem;

        /// <summary>
        /// Variable of OpenFileDialog in form.
        /// </summary>
        private System.Windows.Forms.OpenFileDialog openFileDialog;

        /// <summary>
        /// Circle item of MenuStrip of form.
        /// </summary>
        private System.Windows.Forms.ToolStripMenuItem circleToolStripMenuItem;

        /// <summary>
        /// Triangle item of MenuStrip of form.
        /// </summary>
        private System.Windows.Forms.ToolStripMenuItem triangleToolStripMenuItem;

        /// <summary>
        /// Rectangle item of MenuStrip of form.
        /// </summary>
        private System.Windows.Forms.ToolStripMenuItem rectangleToolStripMenuItem;

        /// <summary>
        /// Variable of Button in form.
        /// </summary>
        private System.Windows.Forms.Button runStopButton;

        /// <summary>
        /// Variable of Button in form.
        /// </summary>
        private System.Windows.Forms.Label languageLabel;

        /// <summary>
        /// Variable of ComboBox in form.
        /// </summary>
        private System.Windows.Forms.ComboBox languageComboBox;

        /// <summary>
        /// Variable of Button in form.
        /// </summary>
        private System.Windows.Forms.Button minusButton;

        /// <summary>
        /// Variable of Button in form.
        /// </summary>
        private System.Windows.Forms.Button plusButton;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FigureMovingAppMainForm));
            this.panelTools = new System.Windows.Forms.Panel();
            this.minusButton = new System.Windows.Forms.Button();
            this.plusButton = new System.Windows.Forms.Button();
            this.languageLabel = new System.Windows.Forms.Label();
            this.languageComboBox = new System.Windows.Forms.ComboBox();
            this.runStopButton = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonCreateRectangle = new System.Windows.Forms.Button();
            this.buttonCreateCircle = new System.Windows.Forms.Button();
            this.buttonCreateTriangle = new System.Windows.Forms.Button();
            this.treeViewFigures = new System.Windows.Forms.TreeView();
            this.pictureBoxMain = new System.Windows.Forms.PictureBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.figureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.circleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.triangleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rectangleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.positionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.panelTools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMain)).BeginInit();
            this.menuStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTools
            // 
            this.panelTools.Controls.Add(this.minusButton);
            this.panelTools.Controls.Add(this.plusButton);
            this.panelTools.Controls.Add(this.languageLabel);
            this.panelTools.Controls.Add(this.languageComboBox);
            this.panelTools.Controls.Add(this.runStopButton);
            this.panelTools.Controls.Add(this.buttonClear);
            this.panelTools.Controls.Add(this.buttonCreateRectangle);
            this.panelTools.Controls.Add(this.buttonCreateCircle);
            this.panelTools.Controls.Add(this.buttonCreateTriangle);
            resources.ApplyResources(this.panelTools, "panelTools");
            this.panelTools.Name = "panelTools";
            // 
            // minusButton
            // 
            resources.ApplyResources(this.minusButton, "minusButton");
            this.minusButton.Name = "minusButton";
            this.minusButton.UseCompatibleTextRendering = true;
            this.minusButton.UseVisualStyleBackColor = true;
            this.minusButton.Click += new System.EventHandler(this.MinusButton_Click);
            // 
            // plusButton
            // 
            resources.ApplyResources(this.plusButton, "plusButton");
            this.plusButton.Name = "plusButton";
            this.plusButton.UseCompatibleTextRendering = true;
            this.plusButton.UseVisualStyleBackColor = true;
            this.plusButton.Click += new System.EventHandler(this.PlusButton_Click);
            // 
            // languageLabel
            // 
            resources.ApplyResources(this.languageLabel, "languageLabel");
            this.languageLabel.Name = "languageLabel";
            // 
            // languageComboBox
            // 
            resources.ApplyResources(this.languageComboBox, "languageComboBox");
            this.languageComboBox.FormattingEnabled = true;
            this.languageComboBox.Name = "languageComboBox";
            this.languageComboBox.SelectedIndexChanged += new System.EventHandler(this.LanguageComboBox_SelectedIndexChanged);
            // 
            // runStopButton
            // 
            resources.ApplyResources(this.runStopButton, "runStopButton");
            this.runStopButton.Name = "runStopButton";
            this.runStopButton.UseVisualStyleBackColor = true;
            this.runStopButton.Click += new System.EventHandler(this.RunStopButton_Click);
            // 
            // buttonClear
            // 
            resources.ApplyResources(this.buttonClear, "buttonClear");
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.ButtonClear_Click);
            // 
            // buttonCreateRectangle
            // 
            resources.ApplyResources(this.buttonCreateRectangle, "buttonCreateRectangle");
            this.buttonCreateRectangle.Name = "buttonCreateRectangle";
            this.buttonCreateRectangle.UseVisualStyleBackColor = true;
            this.buttonCreateRectangle.Click += new System.EventHandler(this.ButtonCreateRectangle_Click);
            this.buttonCreateRectangle.Paint += new System.Windows.Forms.PaintEventHandler(this.RectangleButton_Paint);
            // 
            // buttonCreateCircle
            // 
            resources.ApplyResources(this.buttonCreateCircle, "buttonCreateCircle");
            this.buttonCreateCircle.Name = "buttonCreateCircle";
            this.buttonCreateCircle.UseVisualStyleBackColor = true;
            this.buttonCreateCircle.Click += new System.EventHandler(this.ButtonCreateCircle_Click);
            this.buttonCreateCircle.Paint += new System.Windows.Forms.PaintEventHandler(this.CircleButton_Paint);
            // 
            // buttonCreateTriangle
            // 
            resources.ApplyResources(this.buttonCreateTriangle, "buttonCreateTriangle");
            this.buttonCreateTriangle.Name = "buttonCreateTriangle";
            this.buttonCreateTriangle.UseVisualStyleBackColor = true;
            this.buttonCreateTriangle.Click += new System.EventHandler(this.ButtonCreateTriangle_Click);
            this.buttonCreateTriangle.Paint += new System.Windows.Forms.PaintEventHandler(this.TriangleButton_Paint);
            // 
            // treeViewFigures
            // 
            resources.ApplyResources(this.treeViewFigures, "treeViewFigures");
            this.treeViewFigures.Name = "treeViewFigures";
            this.treeViewFigures.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeViewFigures_AfterSelect);
            // 
            // pictureBoxMain
            // 
            this.pictureBoxMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.pictureBoxMain, "pictureBoxMain");
            this.pictureBoxMain.Name = "pictureBoxMain";
            this.pictureBoxMain.TabStop = false;
            this.pictureBoxMain.Paint += new System.Windows.Forms.PaintEventHandler(this.PictureBoxMain_Paint);
            // 
            // timer
            // 
            this.timer.Interval = 50;
            this.timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // menuStripMain
            // 
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            resources.ApplyResources(this.menuStripMain, "menuStripMain");
            this.menuStripMain.Name = "menuStripMain";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            resources.ApplyResources(this.menuToolStripMenuItem, "menuToolStripMenuItem");
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.figureToolStripMenuItem,
            this.positionToolStripMenuItem});
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            resources.ApplyResources(this.openToolStripMenuItem, "openToolStripMenuItem");
            // 
            // figureToolStripMenuItem
            // 
            this.figureToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.circleToolStripMenuItem,
            this.triangleToolStripMenuItem,
            this.rectangleToolStripMenuItem});
            this.figureToolStripMenuItem.Name = "figureToolStripMenuItem";
            resources.ApplyResources(this.figureToolStripMenuItem, "figureToolStripMenuItem");
            // 
            // circleToolStripMenuItem
            // 
            this.circleToolStripMenuItem.Name = "circleToolStripMenuItem";
            resources.ApplyResources(this.circleToolStripMenuItem, "circleToolStripMenuItem");
            this.circleToolStripMenuItem.Click += new System.EventHandler(this.CircleToolStripMenuItem_Click);
            // 
            // triangleToolStripMenuItem
            // 
            this.triangleToolStripMenuItem.Name = "triangleToolStripMenuItem";
            resources.ApplyResources(this.triangleToolStripMenuItem, "triangleToolStripMenuItem");
            this.triangleToolStripMenuItem.Click += new System.EventHandler(this.TriangleToolStripMenuItem_Click);
            // 
            // rectangleToolStripMenuItem
            // 
            this.rectangleToolStripMenuItem.Name = "rectangleToolStripMenuItem";
            resources.ApplyResources(this.rectangleToolStripMenuItem, "rectangleToolStripMenuItem");
            this.rectangleToolStripMenuItem.Click += new System.EventHandler(this.RectangleToolStripMenuItem_Click);
            // 
            // positionToolStripMenuItem
            // 
            this.positionToolStripMenuItem.Name = "positionToolStripMenuItem";
            resources.ApplyResources(this.positionToolStripMenuItem, "positionToolStripMenuItem");
            this.positionToolStripMenuItem.Click += new System.EventHandler(this.PositionToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            resources.ApplyResources(this.saveToolStripMenuItem, "saveToolStripMenuItem");
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
            // 
            // openFileDialog
            // 
            resources.ApplyResources(this.openFileDialog, "openFileDialog");
            // 
            // FigureMovingAppMainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pictureBoxMain);
            this.Controls.Add(this.treeViewFigures);
            this.Controls.Add(this.panelTools);
            this.Controls.Add(this.menuStripMain);
            this.MainMenuStrip = this.menuStripMain;
            this.Name = "FigureMovingAppMainForm";
            this.panelTools.ResumeLayout(false);
            this.panelTools.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMain)).EndInit();
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}