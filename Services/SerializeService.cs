﻿//-----------------------------------------------------------------------
// <copyright file="SerializeService.cs" company="Digital Cloud Technologies">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------

namespace FigureMovingApp.Services
{
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Xml.Serialization;
    using FigureMovingApp.Common.Enums;
    using Newtonsoft.Json;

    /// <summary>
    /// Serialization service class.
    /// </summary>
    public class SerializeService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SerializeService" /> class.
        /// </summary>
        public SerializeService()
        {
        }

        /// <summary>
        /// Serialize object.
        /// </summary>
        /// <param name="serializedObject">object for serialization.</param>
        /// <param name="serializeFormat">format of file for serialization.</param>
        /// <param name="stream">stream for serialize object as file.</param>
        public void SerializeObject(object serializedObject, SerializeFormat serializeFormat, FileStream stream)
        {
            if (serializeFormat == SerializeFormat.Bin)
            {
                this.SerializeBinary(serializedObject, stream);
            }

            if (serializeFormat == SerializeFormat.Json)
            {
                this.SerializeJson(serializedObject, stream);
            }

            if (serializeFormat == SerializeFormat.XML)
            {
                this.SerializeXml(serializedObject, stream);
            }
        }

        /// <summary>
        /// Deserialize object.
        /// </summary>
        /// <param name="serializeFormat">format of file for deserialization.</param>
        /// <param name="stream">stream of deserialize file.</param>
        /// <typeparam name="TFigureType">type of figure.</typeparam>
        /// <returns>deserialized objet</returns>
        public TFigureType DeserializeObject<TFigureType>(SerializeFormat serializeFormat, FileStream stream)
        {
            if (serializeFormat == SerializeFormat.Bin)
            {
                return this.DeserializeBinary<TFigureType>(stream);
            }
            else if (serializeFormat == SerializeFormat.Json)
            {
                return this.DeserializeJson<TFigureType>(stream);
            }
            else
            {
                return this.DeserializeXml<TFigureType>(stream);
            }
        }

        /// <summary>
        /// Serialize object binary.
        /// </summary>
        /// <param name="serializedObject">object for serialization.</param>
        /// <param name="stream">stream for serialize object as file.</param>
        private void SerializeBinary(object serializedObject, FileStream stream)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, serializedObject);
        }

        /// <summary>
        /// Deserialize object binary.
        /// </summary>
        /// <param name="stream">stream of deserialize file.</param>
        /// <typeparam name="TFigureType">type of figure.</typeparam>
        /// <returns>deserialized objet</returns>
        private TFigureType DeserializeBinary<TFigureType>(FileStream stream)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            var deserealizedObject = (TFigureType)formatter.Deserialize(stream);
            return deserealizedObject;
        }

        /// <summary>
        /// Serialize object Json.
        /// </summary>
        /// <param name="serializedObject">object for serialization.</param>
        /// <param name="stream">stream for serialize object as file.</param>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed.")]
        private void SerializeJson(object serializedObject, FileStream stream)
        {
            var serializer = new JsonSerializer();

            using (StreamWriter sw = new StreamWriter(stream))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, serializedObject);
            }
        }

        /// <summary>
        /// Deserialize object Json.
        /// </summary>
        /// <param name="stream">stream of deserialize file.</param>
        /// <typeparam name="TFigureType">type of figure.</typeparam>
        /// <returns>deserialized objet</returns>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed.")]
        private TFigureType DeserializeJson<TFigureType>(FileStream stream)
        {
            var serializer = new JsonSerializer();

            using (StreamReader sr = new StreamReader(stream))
            using (JsonTextReader reader = new JsonTextReader(sr))
            {
                var deserialiseObject = serializer.Deserialize<TFigureType>(reader);
                return deserialiseObject;  
            }
        }

        /// <summary>
        /// Serialize object xml.
        /// </summary>
        /// <param name="serializedObject">object for serialization.</param>
        /// <param name="stream">stream for serialize object as file.</param>
        private void SerializeXml(object serializedObject, FileStream stream)
        {
            var serializer = new XmlSerializer(serializedObject.GetType());
            serializer.Serialize(stream, serializedObject);
        }

        /// <summary>
        /// Deserialize object xml.
        /// </summary>
        /// <param name="stream">stream of deserialize file.</param>
        /// <typeparam name="TFigureType">type of figure.</typeparam>
        /// <returns>deserialized object</returns>
        private TFigureType DeserializeXml<TFigureType>(FileStream stream)
        {
            var serializer = new XmlSerializer(typeof(TFigureType));

            return (TFigureType)serializer.Deserialize(stream);
        }
    }
}